Application performs ingestion of json files to the 3 locations:
- raw location. Files in this location stored without any changes
- clean location. Files with duplicated records removed
- aggregated location. Result of aggregation stored here.

Paths to the mentioned locations are specified in the application.conf file

Application requires only one parameter: --location. It specifies the location of the input files

There are two options for running the application:

###### Using Spark without Docker (you need to have sbt and spark installed):

  - to build uber jar, run `sbt assembly`
  - run the application using spark-submit script. For example:

        ./bin/spark-submit \
        --class com.yelpdataset.bigdata.takehomeassignment.Assignment \
        --master local[*] \
        /path-to-jar-file/big-data-take-home-assignment-assembly-1.0.jar \
        --location /path-to-source-files/

###### Using Docker container (you need to have docker and sbt installed): 

  - run `sbt docker:publishLocal`, you will get image big-data-take-home-assignment:1.0 published to your local repository
  - run `docker run --mount type=bind,source="/path-to-source-files",target=/tmp/source --rm big-data-take-home-assignment:1.0 --location /tmp/source/`
    Make sure that Docker user has read and write access to the path with source files on the host (or use /tmp directory).
    Output locations will be created at the same directory where source files located.
    Do not change `target` and `--location` parameters (otherwise you will need to update application.conf file)

    
Notes:
Design of the application is the following: spark reads files from the local
file system to dataframes. Then the raw dataframe is stored to the raw location. Next, we remove 
duplicates from the input dataframe and store this clean data as json file to the clean area.
I did not find any problems with data in the source files to perform deeper cleaning.
Some cleaning might be required by business users. For example, removing closed companies etc. It 
could be done by filtering.
I accept that copying files from the source location to raw by spark does not make sense, but
I treat it as a mocking of the real process, when the source might be a remote location or database,
and destination is some distributed file system (HDFS, S3).

After writing the data to clean area, spark reads required files and performs aggregation.
Result stored in aggregated area as a json file.

In this case reading input data for aggregation from files does not make sense as well, but again I treat
it as a mocking of the real process. Most probably in the real life it would be better to have two separate
applications for data ingestion and aggregation.