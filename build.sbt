name := "big-data-take-home-assignment"

organization := "com.testing"

version := "1.0"

scalaVersion := "2.11.12"

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

Compile / mainClass := Some("com.yelpdataset.bigdata.takehomeassignment.Assignment")

//dockerBaseImage := "datamechanics/spark:3.1-latest"
dockerBaseImage := "bitnami/spark:latest"

val versions = new {
  val sparkVersion = "2.4.3"
  val scalaTestVersion = "3.0.5"
  val config = "1.2.1"
  val circe = "0.9.1"
  val circeConfig = "0.4.1"
  val scalaLogging = "3.8.0"
  val sparkTestingBase = "2.4.3_0.14.0"
  val scopt = "3.7.0"
}

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % versions.sparkVersion % Provided,
  "org.apache.spark" %% "spark-sql" % versions.sparkVersion,
  "org.scalatest" %% "scalatest" % versions.scalaTestVersion % Test,
  "com.typesafe" % "config" % versions.config,
  "io.circe" %% "circe-generic" % versions.circe,
  "io.circe" %% "circe-parser" % versions.circe,
  "io.circe" %% "circe-config" % versions.circeConfig,
  "com.typesafe.scala-logging" %% "scala-logging" % versions.scalaLogging,
  "com.github.scopt" %% "scopt" % versions.scopt,
  "com.holdenkarau" %% "spark-testing-base" % versions.sparkTestingBase % Test
)
