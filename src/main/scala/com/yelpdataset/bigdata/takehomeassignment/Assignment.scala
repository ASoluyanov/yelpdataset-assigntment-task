package com.yelpdataset.bigdata.takehomeassignment

import com.yelpdataset.bigdata.takehomeassignment.config.AggregationConfig
import com.typesafe.scalalogging.LazyLogging
import com.yelpdataset.bigdata.takehomeassignment.config.{AggregationConfig, ApplicationConfig, Parameters}

import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import org.apache.spark.sql.functions.{col, udf}
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.log4j.Logger
import org.apache.log4j.Level
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

import scala.util.control.NonFatal
import scala.util.Try

object Assignment extends App with LazyLogging {

  implicit val spark: SparkSession =
    SparkSession.builder().appName("business data processing").config("spark.master", "local").getOrCreate()

  Logger.getLogger("org").setLevel(Level.ERROR)

  lazy val getWeekId: UserDefinedFunction = udf((date: String) => {

    val processingLocalDate: LocalDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    val dayNumber = new SimpleDateFormat("u").format(new SimpleDateFormat("yyyy-MM-dd").parse(date)).toInt
    val sunday = processingLocalDate.plusDays(7 - dayNumber)
    val monday = processingLocalDate.minusDays(dayNumber - 1)
    s"$monday - $sunday"
  })

  val process = for {
    parameters <- Parameters.parse(args)
    applicationConfig <- ApplicationConfig.read()
    aggregationConfig <- AggregationConfig.read()
    _ <- ingestFilesToRawAndCleanup(applicationConfig, parameters)
    aggregatedDf <- aggregateData(applicationConfig, aggregationConfig)
    _ <- Try {
      writeDataFrameAsJson(aggregatedDf, applicationConfig.aggregatedLocation)
    }
  } yield {
    logger.info("Files processing completed")
  }

  process.recover {
    case NonFatal(e) =>
      logger.error("Processing failed", e)
      sys.exit(1)
  }.get

  def ingestFilesToRawAndCleanup(applicationConfig: ApplicationConfig, parameters: Parameters)(
    implicit spark: SparkSession): Try[Unit] = Try {

    applicationConfig.datasets.map { dataset =>
      logger.info(s"reading source file ${parameters.source}${dataset._2}")
      val sourceFile = s"${parameters.source}${dataset._2}"
      val input = spark.read.json(sourceFile)
      writeDataFrameAsJson(input, s"${applicationConfig.rawLocation}${dataset._2}")
      writeDataFrameAsJson(input.distinct(), s"${applicationConfig.cleanLocation}${dataset._2}")
    }
  }

  def aggregateData(applicationConfig: ApplicationConfig, aggregationConfig: AggregationConfig)(
    implicit spark: SparkSession): Try[DataFrame] = Try {

    val baseDF = spark.read.json(s"${applicationConfig.cleanLocation}${applicationConfig.datasets("business")}")

    val reviewDF = spark.read
      .json(s"${applicationConfig.cleanLocation}${applicationConfig.datasets("review")}")
      .withColumn("week_id", getWeekId(col("date")))
      .groupBy("week_id", "business_id")
      .avg("stars")
      .withColumnRenamed("avg(stars)", "avg_stars")

    val baseDfWithWeeklyRatings = baseDF.join(reviewDF, Seq("business_id"), "left")

    val checkinDf = spark.read
      .json(s"${applicationConfig.cleanLocation}${applicationConfig.datasets("checkin")}")
      .withColumn("date", split(col("date"), ", "))
      .withColumn("date", explode(col("date")))
      .withColumn("week_id", getWeekId(col("date")))
      .groupBy("week_id", "business_id")
      .count()

    val baseDfWithRatingsAndCheckouts = baseDfWithWeeklyRatings.join(checkinDf, Seq("week_id", "business_id"), "left")

    val intermediateRight = checkinDf.join(baseDfWithRatingsAndCheckouts, Seq("business_id", "week_id"), "left_anti")

    intermediateRight
      .join(baseDF, Seq("business_id"), "left")
      .withColumn("avg_stars", lit(null))
      .select(aggregationConfig.columnOrder.map(col): _*)
      .union(baseDfWithRatingsAndCheckouts)
  }

  def writeDataFrameAsJson(dataFrame: DataFrame, outputLocation: String): Unit = {
    logger.info(s"writing data to $outputLocation")
    dataFrame.write.mode(SaveMode.Overwrite).json(outputLocation)
  }
}
