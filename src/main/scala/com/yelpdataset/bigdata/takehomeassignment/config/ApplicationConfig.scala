package com.yelpdataset.bigdata.takehomeassignment.config

import ApplicationConfig._
import com.typesafe.config.{Config, ConfigFactory}

import scala.util.{Failure, Success, Try}

case class ApplicationConfig(rawLocation: String,
                             cleanLocation: String,
                             aggregatedLocation:String,
                             datasets: Map[String, String])

object ApplicationConfig {

  import io.circe.config.syntax._
  import io.circe.generic.auto._

  def read(): Try[ApplicationConfig] =
    Try {
      ConfigFactory
        .parseResources("application.conf")
        .getConfig("yelpdataset.transactions.application")
        .resolve()
    }.flatMap {
        _.as[ApplicationConfig].fold(Failure(_), Success(_))
      }
      .recoverWith {
        case e => Failure(new IllegalArgumentException(s"Cannot read configuration for transformations", e))
      }
}
