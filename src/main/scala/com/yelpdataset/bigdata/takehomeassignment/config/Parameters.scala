package com.yelpdataset.bigdata.takehomeassignment.config

import scopt.OptionParser

import scala.util.{Failure, Success, Try}

case class Parameters(source: String = "")

object Parameters {

  def parse(args: Array[String]): Try[Parameters] = {

    new OptionParser[Parameters]("source files location") {

      opt[String]('l', "location")
        .required()
        .valueName("<transformation>")
        .action((x, c) => c.copy(source = x))
        .text("Source location")
    }.parse(args, Parameters()) match {
      case Some(x) => Success(x)
      case None =>
        Failure(new IllegalArgumentException("Error parsing arguments : " + args.mkString("[", ", ", "]")))
    }
  }
}
