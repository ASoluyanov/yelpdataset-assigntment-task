package com.yelpdataset.bigdata.takehomeassignment.config

import com.typesafe.config.ConfigFactory
import scala.util.{Failure, Success, Try}

case class AggregationConfig(columnOrder: List[String])

object AggregationConfig {

  import io.circe.config.syntax._
  import io.circe.generic.auto._

  def read(): Try[AggregationConfig] =
    Try {
      ConfigFactory
        .parseResources("aggregation.conf")
        .getConfig("yelpdataset.business.aggregations")
        .resolve()
    }.flatMap {
        _.as[AggregationConfig].fold(Failure(_), Success(_))
      }
      .recoverWith {
        case e => Failure(new IllegalArgumentException(s"Cannot read configuration for aggregation", e))
      }
}
