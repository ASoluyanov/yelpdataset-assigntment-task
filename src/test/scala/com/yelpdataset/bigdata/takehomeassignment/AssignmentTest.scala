package com.yelpdataset.bigdata.takehomeassignment

import org.scalatest._
import com.holdenkarau.spark.testing.DataFrameSuiteBase
import Assignment.getWeekId
import com.yelpdataset.bigdata.takehomeassignment.config.AggregationConfig
import com.yelpdataset.bigdata.takehomeassignment.config.{AggregationConfig, ApplicationConfig}
import org.apache.spark.sql.types.{StructField, StructType}
import org.apache.spark.sql.Row
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types._

class AssignmentTest extends WordSpec with Matchers with DataFrameSuiteBase {

  "Assignment" should {

    val inputPath = getClass.getResource("/input_files").getPath
    val applicationConfig = ApplicationConfig
      .read()
      .get
      .copy(
        rawLocation = s"$inputPath/raw/",
        cleanLocation = s"$inputPath/clean/",
        aggregatedLocation = s"$inputPath/aggregated/")

    val aggregationConfig = AggregationConfig.read().get

    "define the borders of the week (week_id) for any day" in {
      import spark.implicits._
      val input = sc
        .parallelize(List(
          "2015-08-31 00:08:27",
          "2015-04-07 00:08:27",
          "2015-07-01 00:08:27",
          "2015-11-26 00:08:27",
          "2015-03-06 00:08:27",
          "2015-08-08 00:08:27",
          "2015-05-31 00:08:27"
        ))
        .toDF("date")
      input.withColumn("week_id", getWeekId(col("date"))).collect() should contain theSameElementsAs
        Seq(
          Row("2015-08-31 00:08:27", "2015-08-31 - 2015-09-06"),
          Row("2015-04-07 00:08:27", "2015-04-06 - 2015-04-12"),
          Row("2015-07-01 00:08:27", "2015-06-29 - 2015-07-05"),
          Row("2015-11-26 00:08:27", "2015-11-23 - 2015-11-29"),
          Row("2015-03-06 00:08:27", "2015-03-02 - 2015-03-08"),
          Row("2015-08-08 00:08:27", "2015-08-03 - 2015-08-09"),
          Row("2015-05-31 00:08:27", "2015-05-25 - 2015-05-31")
        )
    }

    "aggregate data" in {
      val result = Assignment.aggregateData(applicationConfig, aggregationConfig)(spark).get.persist()
      result.show(false)

      result.schema shouldBe StructType(
        Seq(
          StructField("week_id", StringType),
          StructField("business_id", StringType),
          StructField("address", StringType),
          StructField(
            "attributes",
            StructType(
              Seq(
                StructField("Alcohol", StringType),
                StructField("Ambience", StringType),
                StructField("BikeParking", StringType),
                StructField("BusinessAcceptsBitcoin", StringType),
                StructField("BusinessAcceptsCreditCards", StringType),
                StructField("BusinessParking", StringType),
                StructField("ByAppointmentOnly", StringType),
                StructField("Caters", StringType),
                StructField("DogsAllowed", StringType),
                StructField("GoodForKids", StringType),
                StructField("GoodForMeal", StringType),
                StructField("HappyHour", StringType),
                StructField("HasTV", StringType),
                StructField("NoiseLevel", StringType),
                StructField("OutdoorSeating", StringType),
                StructField("RestaurantsAttire", StringType),
                StructField("RestaurantsDelivery", StringType),
                StructField("RestaurantsGoodForGroups", StringType),
                StructField("RestaurantsPriceRange2", StringType),
                StructField("RestaurantsReservations", StringType),
                StructField("RestaurantsTableService", StringType),
                StructField("RestaurantsTakeOut", StringType),
                StructField("WheelchairAccessible", StringType),
                StructField("WiFi", StringType)
              )
            )
          ),
          StructField("categories", StringType),
          StructField("city", StringType),
          StructField(
            "hours",
            StructType(
              Seq(
                StructField("Friday", StringType),
                StructField("Monday", StringType),
                StructField("Saturday", StringType),
                StructField("Sunday", StringType),
                StructField("Thursday", StringType),
                StructField("Tuesday", StringType),
                StructField("Wednesday", StringType)
              )
            )
          ),
          StructField("is_open", LongType),
          StructField("latitude", DoubleType),
          StructField("longitude", DoubleType),
          StructField("name", StringType),
          StructField("postal_code", StringType),
          StructField("review_count", LongType),
          StructField("stars", DoubleType),
          StructField("state", StringType),
          StructField("avg_stars", DoubleType),
          StructField("count", LongType)
        )
      )

      result.select("week_id", "business_id", "avg_stars", "count").collect() should contain theSameElementsAs
        Seq(
          Row("2018-09-10 - 2018-09-16", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2017-11-20 - 2017-11-26", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2015-03-16 - 2015-03-22", "oaepsyvc0J17qwi8cfrOWg", null, 1),
          Row("2013-12-23 - 2013-12-29", "PE9uqAjdw0E4-8mjGl3wVA", null, 1),
          Row("2015-01-26 - 2015-02-01", "oaepsyvc0J17qwi8cfrOWg", null, 1),
          Row("2012-09-17 - 2012-09-23", "bvN78flM8NLprQ1a1y5dRg", null, 1),
          Row("2010-12-20 - 2010-12-26", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2010-02-22 - 2010-02-28", "bvN78flM8NLprQ1a1y5dRg", null, 2),
          Row("2011-08-15 - 2011-08-21", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2015-02-16 - 2015-02-22", "oaepsyvc0J17qwi8cfrOWg", null, 1),
          Row("2016-06-06 - 2016-06-12", "PE9uqAjdw0E4-8mjGl3wVA", null, 2),
          Row("2014-11-03 - 2014-11-09", "oaepsyvc0J17qwi8cfrOWg", null, 1),
          Row("2010-12-13 - 2010-12-19", "PE9uqAjdw0E4-8mjGl3wVA", null, 1),
          Row("2015-10-12 - 2015-10-18", "PE9uqAjdw0E4-8mjGl3wVA", null, 1),
          Row("2012-08-27 - 2012-09-02", "bvN78flM8NLprQ1a1y5dRg", null, 1),
          Row("2012-10-08 - 2012-10-14", "PE9uqAjdw0E4-8mjGl3wVA", null, 2),
          Row("2015-11-16 - 2015-11-22", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2010-03-01 - 2010-03-07", "bvN78flM8NLprQ1a1y5dRg", null, 1),
          Row("2010-11-01 - 2010-11-07", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2012-09-17 - 2012-09-23", "PE9uqAjdw0E4-8mjGl3wVA", null, 1),
          Row("2014-07-28 - 2014-08-03", "oaepsyvc0J17qwi8cfrOWg", null, 1),
          Row("2010-10-04 - 2010-10-10", "tCbdrRPZA0oiIYSmHG3J0w", null, 1),
          Row("2014-05-05 - 2014-05-11", "6iYb2HFDywm3zjuRg0shjw", 5.0, 1),
          Row("2014-10-06 - 2014-10-12", "6iYb2HFDywm3zjuRg0shjw", 4.0, null),
          Row("2015-06-29 - 2015-07-05", "tCbdrRPZA0oiIYSmHG3J0w", 4.0, null),
          Row("2012-05-28 - 2012-06-03", "tCbdrRPZA0oiIYSmHG3J0w", 4.0, null),
          Row("2017-11-27 - 2017-12-03", "bvN78flM8NLprQ1a1y5dRg", 5.0, null),
          Row("2013-05-27 - 2013-06-02", "bvN78flM8NLprQ1a1y5dRg", 5.0, null),
          Row("2006-04-10 - 2006-04-16", "oaepsyvc0J17qwi8cfrOWg", 2.0, null),
          Row("2010-01-04 - 2010-01-10", "oaepsyvc0J17qwi8cfrOWg", 2.0, null),
          Row("2018-01-15 - 2018-01-21", "PE9uqAjdw0E4-8mjGl3wVA", 1.0, null),
          Row("2011-07-25 - 2011-07-31", "PE9uqAjdw0E4-8mjGl3wVA", 4.0, null)
        )
    }
  }
}
